"""gong target sink class, which handles writing streams."""


from typing import Any, Dict, List, Optional

from target_gong.rest import Rest
from singer_sdk.sinks import RecordSink
import abc
from singer_sdk.plugin_base import PluginBase

from target_gong.auth import OAuth2Authenticator




class gongSink(RecordSink, Rest):
    """wix target sink class."""
    auth_endpoint = "https://app.gong.io/oauth2/generate-token"

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        """Initialize target sink."""
        self._target = target
        super().__init__(target, stream_name, schema, key_properties)

    
    @property
    def unified_schema(self):
        raise NotImplementedError

    @property
    def endpoint(self):
        raise NotImplementedError

       
    @property
    def authenticator(self) -> OAuth2Authenticator:
        return OAuth2Authenticator(self._target, self.auth_endpoint)

    def validate_input(self, record: dict):
        return self.unified_schema(**record).dict()
    
    def validate_output(self, mapping):
        payload = self.clean_payload(mapping)
       
        return payload
   
    def url(self):
        url = "https://api.gong.io/v2/"
        return url 

    


   

    

    


    

    