"""gong target sink class, which handles writing streams."""

from __future__ import annotations

from target_gong.client import gongSink

from singer_sdk.sinks import RecordSink
from target_gong.rest import Rest
from hotglue_models_crm.crm import Activity
class ActivitiesSink(gongSink,Rest):
    """gong target sink class."""

    name = "Activities"
    unified_schema = Activity
    endpoint = "calls/"
    

    def preprocess_record(self, record: dict, context: dict) -> dict:
        record = self.validate_input(record)
        mapping = {
        "actualStart": record.get("created_at"),
        "clientUniqueId": record.get("id"),
        "direction": "Conference",
        "disposition": "No Answer",
        "duration": float(record.get("duration_seconds")),
        "parties": [
            {
            "userId": "6237266230968413345",
            }
        ],
        "primaryUser": "6237266230968413345",
        "purpose": record.get("note"),
        "scheduledEnd": record.get("end_datetime"),
        "scheduledStart": record.get("start_datetime"),
        "title": record.get('title'),
        "downloadMediaUrl" : record.get("recording_url"),
        "meetingUrl" : record.get("meeting_url")
        }
        return  self.validate_output(mapping)

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
       
        response = self.request_api("POST",self.endpoint,request_data=record)
        try:
            id = response.json().get("callId")
            self.logger.info(f"{self.name} created with id: {id}")    
        except:
            self.logger.info(f"{self.name} not created")

       
