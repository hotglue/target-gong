"""wix target class."""

from __future__ import annotations
from typing import Type

from singer_sdk.target_base import Target
from singer_sdk import typing as th
from singer_sdk.sinks import Sink
from target_gong.sinks import (
    
    ActivitiesSink
)

SINK_TYPES = [ActivitiesSink]

class Targetgong(Target):
    """Sample target for gong"""

    
    def __init__(
        self,
        config=None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, parse_env_config, validate_config)

    name = "target-gong"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id",th.StringType, required=True),
        th.Property("client_secret",th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
    ).to_dict()

    
    def get_sink_class(self, stream_name: str) -> Type[Sink]:
        """Get sink for a stream."""
        return next(
            (
                sink_class
                for sink_class in SINK_TYPES
                if sink_class.name.lower() == stream_name.lower()
            ),
            None,
        )


if __name__ == "__main__":
    Targetgong.cli()
