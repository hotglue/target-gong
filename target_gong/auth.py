"""gong Authentication."""

import json
from datetime import datetime
from typing import Any, Dict, Mapping, Optional
from types import MappingProxyType
from requests.auth import HTTPBasicAuth
import logging

import requests


class OAuth2Authenticator:
    """API Authenticator for OAuth 2.0 flows."""

    def __init__(
        self,
        target,
        auth_endpoint: Optional[str] = None,
    ) -> None:
        """Init authenticator.

        Args:
            stream: A stream for a RESTful endpoint.
        """
        self.target_name: str = target.name
        self._config: Dict[str, Any] = dict(target.config)
        self._auth_headers: Dict[str, Any] = {}
        self._auth_params: Dict[str, Any] = {}
        self.logger: logging.Logger = target.logger
        self._auth_endpoint = auth_endpoint
        self._config_file = target.config_file
        self._target = target
        self.update_access_token()
        

    @property
    def config(self) -> Mapping[str, Any]:
        """Get stream or tap config."""
        return MappingProxyType(self._config)

    @property
    def auth_headers(self) -> dict:
        """Return a dictionary of auth headers to be applied.

        These will be merged with any `http_headers` specified in the stream.

        Returns:
            HTTP headers for authentication.
        """
        result = {}
        result["Content-Type"] = "application/json"
        if not self.is_token_valid():
            self.update_access_token()
        result["Authorization"] = f"Bearer {self.access_token}"
        return result

    @property
    def auth_endpoint(self) -> str:
        """Get the authorization endpoint.

        Returns:
            The API authorization endpoint if it is set.

        Raises:
            ValueError: If the endpoint is not set.
        """
        if not self._auth_endpoint:
            raise ValueError("Authorization endpoint not set.")
        return self._auth_endpoint

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the gong API."""
        return {
            "grant_type": "refresh_token",
            "client_id": self._target._config["client_id"],
            "client_secret": self._target._config["client_secret"],
            "refresh_token": self._target._config["refresh_token"],
        }

    
    @property
    def oauth_params(self) -> dict:
        params = {}
        params["grant_type"] = "refresh_token"
        params["refresh_token"] = self._target._config["refresh_token"]

        return params


    @property
    def oauth_request_payload(self) -> dict:
        """Get request body.

        Returns:
            A plain (OAuth) or encrypted (JWT) request body.
        """
        return self.oauth_request_body

    # Authentication and refresh
    def update_access_token(self) -> None:
        """Update `access_token` along with: `last_refreshed` and `expires_in`.

        Raises:
            RuntimeError: When OAuth login fails.
        """
        request_time = round(datetime.utcnow().timestamp())

        client_secret = self._target._config["client_secret"]
        client_id = self._target._config["client_id"]

        token_response = requests.post(
            self.auth_endpoint,
            params=self.oauth_params,
            auth=HTTPBasicAuth(client_id, client_secret),
        )
        try:
            token_response.raise_for_status()
            self.logger.info("OAuth authorization attempt was successful.")
        except Exception as ex:
            raise RuntimeError(
                f"Failed OAuth login, response was '{token_response.json()}'. {ex}"
            )
        token_json = token_response.json()
        self.access_token = token_json["access_token"]
        expires_in = request_time + token_json["expires_in"]

        self._target._config["access_token"] = token_json["access_token"]
        self._target._config["expires_in"] = expires_in
        self._target._config["refresh_token"] = token_json["refresh_token"]
        with open(self._target.config_file, "w") as outfile:
            json.dump(self._target._config, outfile, indent=4)

    def is_token_valid(self) -> bool:

       return True
